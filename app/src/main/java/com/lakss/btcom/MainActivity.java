package com.lakss.btcom;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private int[] list; public int[] getList(){return this.list;} public void setList(int index, int value){this.list[index] = value;}
    private BluetoothAdapter BTadapter;
    private final int REQUEST_BT_ENABLED = 12;



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK){
            System.out.println("BT enabled, RESULT_OK");
        }else{
            System.out.println("BT disabled, RESULT_CANCELED");
        }



    }

    public void syncToDevice(View v){
        System.out.println("syncToDevice() reached!");

        //Check if BT is enabled
        if(!this.BTadapter.isEnabled()){
            Intent btIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(btIntent, this.REQUEST_BT_ENABLED);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       //BTadapter
        this.BTadapter = BluetoothAdapter.getDefaultAdapter();
        if(BTadapter != null)
            System.out.println("BT adapter found!");
        else
            System.out.println("Device doesn't support BT!");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
